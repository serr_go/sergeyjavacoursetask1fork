package com.sergey.javacourse.task1;

import java.util.ArrayList;

public class Weather {
    private String count;
    private String created;
    private String lang;
    private Results results;

    public Results getResults() {
        return results;
    }

    public class Results{
        private ArrayList<Channel> channel;

        public ArrayList<Channel> getChannel() {
            return channel;
        }

        public class Channel{
            private Item item;

            public Item getItem(){
                return item;
            }

            public class Item{
                private Forecast forecast;

                public Forecast getForecast() {
                    return forecast;
                }

                public class Forecast{
                    private int code;
                    private String date;
                    private String day;
                    private int high;
                    private int low;
                    private String text;

                    public int getCode() {
                        return code;
                    }

                    public String getDate() {
                        return date;
                    }

                    public String getDay(){
                        return day;
                    }

                    public int getHigh(){
                        return high;
                    }

                    public int getLow(){
                        return low;
                    }

                    /*public String getText() {
                        return text;
                    }*/

                    public int getMaxTempCel(){
                        return (int) ((high-32)*5./9);
                    }

                    public int getMinTempCel(){
                        return (int) ((low-32)*5./9);
                    }
                }
            }
        }
    }
}
