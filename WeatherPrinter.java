package com.sergey.javacourse.task1;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class WeatherPrinter {
    public void print(WeatherStorage weatherForGomel){
        FileWriter printer = null;
        WeatherLocale locale = new WeatherLocale();

        try {
            printer = new FileWriter("weather.csv", false);
        } catch (IOException e) {
            e.printStackTrace();
        }

        ArrayList<Weather.Results.Channel> channel = weatherForGomel.getStorage().getQuery().getResults().getChannel();

        for ( Weather.Results.Channel rec: channel){
            Weather.Results.Channel.Item.Forecast forecast = rec.getItem().getForecast();
            try {
                printer.write(forecast.getDay()+";"+
                        forecast.getDate()+";"+
                        forecast.getMaxTempCel()+";"+
                        forecast.getMinTempCel()+";"+
                        locale.getLocaleText(Integer.toString(forecast.getCode())));
                printer.write(System.lineSeparator());
            } catch (IOException e) {
                System.out.println("Error printing in file.");
                e.printStackTrace();
            }
        }

        try {
            printer.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
